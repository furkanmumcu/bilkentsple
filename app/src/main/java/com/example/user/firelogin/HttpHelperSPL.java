package com.example.user.firelogin;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;


public class HttpHelperSPL  extends AppCompatActivity {

    public static ProductSPL GetProduct(String barcode, String[] ingredients) throws Exception {
            BufferedReader in = null;
            Gson gson = new Gson();
            String data = null;
            HttpClient httpclient = new DefaultHttpClient();

            HttpGet request = new HttpGet();
            URI website = new URI("http://192.168.200.90:99/api/products/"+barcode);
            request.setURI(website);
            HttpResponse response = httpclient.execute(request);
            in = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));
            String line = in.readLine();
            ProductSPL prod = gson.fromJson(line, ProductSPL.class);


            for(int i = 0; i < ingredients.length; i++)
                prod.nutriments.get(ingredients[i]);


            if (prod != null)
                return prod;
            else
                return new ProductSPL();
        }
    }