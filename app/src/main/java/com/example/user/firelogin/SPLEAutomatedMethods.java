package com.example.user.firelogin;

import android.os.Handler;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SPLEAutomatedMethods extends BarcodeMainActivity{


    public double[] findIngredientResult(final String[] ingredients) {

        final double[] result = new double[ingredients.length];

        for (int i = 0; i < ingredients.length; i++) {
            final int finalI = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String resultc = resultCode;
                        ProductSPL product = HttpHelperSPL.GetProduct(resultc, ingredients); //resultCode
                        String value = product.GetIngredient(ingredients[finalI]);
                        result[finalI] = Double.parseDouble(value);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
        return result;
    }


    public boolean[] findDiseases(final String[] diseases){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference readData;
        final boolean[] result = new boolean[diseases.length];
        for(int i = 0; i < diseases.length; i++) {
            readData = FirebaseDatabase.getInstance().getReference(user.getUid()).child(diseases[i]);
            final int finalI = i;
            readData.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Boolean checkStatus = (Boolean) dataSnapshot.getValue();
                    if(checkStatus != null) {
                        result[finalI] = checkStatus;
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                }
            });
        }
        return result;
    }


    public void run(int age, int height, int weight, String gender, int[] age_rules, int[] height_rules, int[] weight_rules,
                    String[] ingredients, String[] diseases, String[] results) {

    }





}
