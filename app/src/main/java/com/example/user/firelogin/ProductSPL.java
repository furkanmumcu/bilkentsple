package com.example.user.firelogin;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ProductSPL {
    @SerializedName("code")
    public String Code;


    @SerializedName("ingredients_text_with_allergens")
    public String ingredients_text_with_allergens;

    @SerializedName("nutrition_grades")
    public String nutrition_grades ;

    @SerializedName("nutriments")
    public Map<String,String> nutriments ;


    public  String GetIngredient(String ingredientName)
    {
        return   this.nutriments.get(ingredientName);
    }

}
