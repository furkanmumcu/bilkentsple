package com.example.user.firelogin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class SuggestionActivitySPLE extends BarcodeMainActivity {

    static boolean woman = false, man = false;
    static int age = 0;
    static double height = 0.0, weight = 0.0;
    int energy = 0, proteins = 0, fat = 0, carbonhydrates = 0, sugars = 0;
    SPLEAutomatedMethods spleAutomatedMethods = new SPLEAutomatedMethods();

    static TextView textView1;
    static TextView textView2;
    static TextView textView3;
    static TextView textView4;
    static TextView textView5;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    DatabaseReference readData1;
    DatabaseReference readData2;
    DatabaseReference readData3;
    DatabaseReference readData4;
    DatabaseReference readData9boolean1;
    DatabaseReference readData10boolean2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);

        textView1 = findViewById(R.id.textEdit1);
        textView2 = findViewById(R.id.textEdit2);
        textView3 = findViewById(R.id.textEdit3);
        textView4 = findViewById(R.id.textEdit4);
        textView5 = findViewById(R.id.textEdit5);


        readData1 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("nameAndSurname");
        readData1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        readData2 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("age");
        readData2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                age = Integer.parseInt(value);
                Log.v("data age", ""+ age);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        readData3 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("height");
        readData3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                height = Double.parseDouble(value);
                Log.v("data height", ""+ height);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        readData4 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("weight");
        readData4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                weight = Double.parseDouble(value);
                Log.v("data weight", ""+ weight);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });


        readData9boolean1 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("female");
        readData9boolean1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Boolean checkStatus = (Boolean) dataSnapshot.getValue();

                if(checkStatus != null) {
                    woman = checkStatus;
                    Log.v("data woman", ""+ woman);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        readData10boolean2 = FirebaseDatabase.getInstance().getReference(user.getUid()).child("male");
        readData10boolean2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Boolean checkStatus = (Boolean) dataSnapshot.getValue();

                if(checkStatus != null) {
                    man = checkStatus;
                    Log.v("data man", ""+ man);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });


        Button button1 = findViewById(R.id.button1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(SuggestionActivitySPLE.this, BarcodeMainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        Button button2 = findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(SuggestionActivitySPLE.this, MainMenu.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        String[] diseases = {"diabetes", "hearthdisease", "cholesterol", "diet"};
        final boolean[] diseasesResult = spleAutomatedMethods.findDiseases(diseases);

        String[] ingred = {"energy", "proteins", "fat", "carbonhydrates", "sugars"};
        final double[] ingredResult =  spleAutomatedMethods.findIngredientResult(ingred);



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(age < 18 || age >= 80) {

                } else if (age >= 18 && age < 32) {

                } else if (age >= 32 && age < 55) {
                    energy += 1;
                    proteins += 1;
                    fat += 1;
                    carbonhydrates += 1;
                    sugars += 1;
                } else {
                    energy += 2;
                    proteins += 2;
                    fat += 2;
                    carbonhydrates += 2;
                    sugars += 2;
                }


                if (weight / (height * height) < 20.0) {
                    energy -= 1;
                    proteins -= 1;
                    fat -= 1;
                    carbonhydrates -= 1;
                    sugars -= 1;
                } else if (weight / (height * height) > 30.0) {
                    energy += 1;
                    proteins += 1;
                    fat += 1;
                    carbonhydrates += 1;
                    sugars += 1;
                }


                if (diseasesResult[0]) {
                    carbonhydrates += 2;
                    sugars += 2;
                }
                if (diseasesResult[1]) {
                    fat += 2;
                    carbonhydrates += 2;
                }
                if (diseasesResult[2]) {
                    fat += 2;
                }
                if (diseasesResult[3]) {
                    if (!(weight / (height * height) < 20.0))
                        fat += 2;
                    sugars += 2;
                }

                energy += ingredResult[0] / 420;
                proteins += (ingredResult[1] * 100 / 80) / 10;
                fat += (ingredResult[2] * 100 / 80) / 10;
                carbonhydrates += ingredResult[3] / 20;
                sugars += ingredResult[4] / 10;

                if(woman == true) {
                    energy += 1;
                    proteins += 1;
                    fat += 1;
                    carbonhydrates += 1;
                    sugars += 1;
                }

                Log.v("last message", "degerler: " + energy + " " + proteins + " " + fat + " " + carbonhydrates + " " + sugars + " ");

                textView1.setText(""+energy);
                textView2.setText(""+proteins);
                textView3.setText(""+fat);
                textView4.setText(""+carbonhydrates);
                textView5.setText(""+sugars);

            }
        }, 4000);
    }

    public static void regulator(int age) {

    }
}

